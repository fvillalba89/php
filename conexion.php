<?php
$servername = "localhost";
$username = "username";
$password = "password";

try {
 // Crear conexión
    $conn = new PDO("mysql:host=$servername;dbname=mydb", $username, $password);
    
 // Establecer el modo de error PDO a excepción
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Conexión establecida."; 
    } catch(PDOException $e)
    {
    echo "Ups! Parece que no se ha podido conectar a la DB.: " . $e->getMessage();
    }
?>